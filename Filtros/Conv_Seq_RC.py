import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import convolve

# Generar una secuencia de 1 y 0
sequence_length = 50
sequence = np.random.choice([0, 1], size=sequence_length)

# Crear una figura y ejes para la secuencia de 1 y 0
plt.subplot(3, 1, 1)
plt.stem(sequence, markerfmt='ro', linefmt='r-', basefmt='k-')
plt.title('Secuencia de 1 y 0')
plt.xlabel('Tiempo')
plt.ylabel('Amplitud')

# Generar un coseno realzado
def coseno_realzado(x, beta):
    return np.cos(np.pi * x) * np.exp(-beta * x)

x_values = np.linspace(0, 4, 1000)
coseno = 5*coseno_realzado(x_values, beta=0.5)

# Crear una figura y ejes para el coseno realzado
plt.subplot(3, 1, 2)
plt.plot(x_values, coseno, 'b-')
plt.title('Coseno Realzado')
plt.xlabel('Tiempo')
plt.ylabel('Amplitud')

# Convolución de la secuencia y el coseno realzado
convolution_result = convolve(sequence, coseno, mode='full')

# Crear una figura y ejes para la convolución
plt.subplot(3, 1, 3)
plt.stem(convolution_result, markerfmt='go', linefmt='g-', basefmt='k-')
plt.title('Convolución de la Secuencia y el Coseno Realzado')
plt.xlabel('Tiempo')
plt.ylabel('Amplitud')

# Ajustar el diseño de la figura
plt.tight_layout()

# Mostrar los gráficos
plt.show()
