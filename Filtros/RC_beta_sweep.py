import numpy as np
import matplotlib.pyplot as plt

def coseno_realzado(x, beta):
    return np.cos(np.pi * x) * np.exp(-beta * x)

beta_values = np.arange(0, 1.2, 0.2)

plt.figure(figsize=(8, 6))

for beta in beta_values:
    x_values = np.linspace(0, 4, 1000)
    y_values = coseno_realzado(x_values, beta)
    plt.plot(x_values, y_values, label=f'Beta = {beta:.1f}')

plt.title('Coseno Realzado para Diferentes Valores de Beta')
plt.xlabel('X')
plt.ylabel('Coseno Realzado')
plt.legend()
plt.grid(True)

# Mostrar el grafico
plt.show()
