import argparse

def crear_archivo_c(nombre_archivo):
    # Nombre del archivo con la extensión .c
    archivo = f"{nombre_archivo}.c"
    
    # Plantilla básica del archivo C
    plantilla = """\
#include <stdio.h>

// Función principal
int main() {
    printf("¡Hola, mundo!\\n");
    return 0;
}
"""
    try:
        # Abrir el archivo en modo escritura y escribir la plantilla
        with open(archivo, "w") as f:
            f.write(plantilla)
        print(f"El archivo '{archivo}' ha sido creado exitosamente.")
    except Exception as e:
        print(f"Ocurrió un error al crear el archivo: {e}")

def main():
    # Configurar argparse para manejar los argumentos de línea de comandos
    parser = argparse.ArgumentParser(description="Script para generar un archivo .c con una plantilla básica.")
    parser.add_argument(
        "-n", "--nombre",
        required=True,
        help="Nombre del archivo sin extensión (se añadirá automáticamente .c)"
    )
    
    # Parsear los argumentos
    args = parser.parse_args()
    
    # Llamar a la función para crear el archivo .c
    crear_archivo_c(args.nombre)

if __name__ == "__main__":
    main()
