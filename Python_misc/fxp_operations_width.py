import os

os.system('clear')
def op_width(p1=0,r1=0,p2=0,r2=0):
  #Sp1.r1 + Sp2.r2 = Sp.r
  r = r2 if r2>r1 else r1
  aux = p1-r1+1 if p1-r1>p2-r2 else p2-r2+1 
  p = aux + r

  print('SUM: S{}.{} + S{}.{} = S{}.{}'.format(p1,r1,p2,r2,p,r))
  print('MUL: S{}.{} x S{}.{} = S{}.{}'.format(p1,r1,p2,r2,p1+p2,r1+r2))
