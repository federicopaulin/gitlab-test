import os
import random

def fxp_min_max_gen(W=1, I=0, vec_len=1, sign='U'): 
  min_max_vec = [0]*vec_len
  if I == 0:
    if sign == "U":
      max_range = (2**W)-1
      print('U{}.0 >>> From 0 to {}\n'.format(W,max_range))
      for ii in range(vec_len): 
        min_max_vec[ii] = random.choice([0, max_range])
    else:
      max_range = 2**(W-1)
      print('S{}.0 >>> From {} to {}\n'.format(W,-max_range,max_range-1))
      for ii in range(vec_len):
         min_max_vec[ii] = random.choice([-max_range, max_range-1])
  else:
    Q = 2**(I-W)
    if sign == "U":
      max_range = (2**I)-Q
      print('U{}.{} >>> From 0 to {}\n'.format(W,I,max_range))
      for ii in range(vec_len):
         min_max_vec[ii] = random.choice([0, max_range])
    else:
      max_range = 2**(I-1)
      print('S{}.{} >>> From {} to {}\n'.format(W,I,float(-max_range),float(max_range-Q)))
      for ii in range(vec_len):
         min_max_vec[ii] = random.choice([-max_range, max_range-Q])
    print('Quantum: {}\n'.format(Q))
  
  print('Min/Max vector: {}\n'.format(min_max_vec))


