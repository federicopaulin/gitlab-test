import os
import random
import fxp_operations_width as fxp_opw
import fxp_gen

os.system('clear')

while True:
  print('\n\t\tFIXED POINT\n')
  print('1) FXP generator\n')
  print('2) FXP operation width\n')
  print('3) Quit\n')

  sel = int(input("Choose an option:"))
  if sel == 1:
    os.system('clear')
    print('\n=================== FXP GENERATOR ===================')
    int_width  = int(input("Integer bits length    :"))
    frac_width = int(input("Fractional bits length :"))
    sign       =     input("Signed? (S/U)          :")
    num_elem   = int(input("Number of elements     :"))
    print('\n')
    fxp_gen.fxp_min_max_gen(int_width,frac_width,num_elem,sign)
    print('=========================================================')
  elif sel == 2:
    os.system('clear')
    print('\n=================== FXP WIDTH OPERATION ===================')
    print('\nFirst FXP number >>> Sp1.r1')
    p1 = int(input('p1:'))
    r1 = int(input('r1:'))
    print('\nSecond FXP number >>> Sp2.r2')
    p2 = int(input('p2:'))
    r2 = int(input('r2:'))
    print('\n')
    fxp_opw.op_width(p1,r1,p2,r2)
    print('=========================================================')
  elif sel == 3:
    os.system('clear')
    print('Quitting ...')
    break
  else:
    os.system('clear')
    print('\t \t NOT A VALID OPTION')